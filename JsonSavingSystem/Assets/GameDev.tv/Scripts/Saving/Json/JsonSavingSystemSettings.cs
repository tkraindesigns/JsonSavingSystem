﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace GameDev.tv.Saving.Json
{
    public class JsonSavingSystemSettings : ScriptableObject
    {
        private const string settingsPath = "Assets/Resources/JsonSavingSystemSettings.asset";
        private const string settingsName = "JsonSavingSystemSettings";
        
        private static JsonSavingSystemSettings instance;

        public static JsonSavingSystemSettings Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = Resources.Load<JsonSavingSystemSettings>(settingsName);
                    #if UNITY_EDITOR
                    if (instance == null)
                    {
                        Debug.Log($"Creating new JsonSavingSystemSettings asset at {settingsPath}");
                        instance = ScriptableObject.CreateInstance<JsonSavingSystemSettings>();
                        AssetDatabase.CreateAsset(instance, settingsPath);
                        AssetDatabase.SaveAssets();
                    } 
                    #endif
                }
                return instance;
            }
        }
        
        [field: SerializeField] public string fileExtension { get; private set; } = "json";
        [field: SerializeField] public bool useEncryption { get; private set; } = false;
        [field: SerializeField] public string encryptionKey { get; private set; } = "";
        [field: SerializeField] public bool useBase64 { get; private set; } = false;
        
        #if UNITY_EDITOR
        public void SetFileExtension(string extension) => fileExtension = extension;
        public void SetUseEncryption(bool value) => useEncryption = value;
        public void SetEncryptionKey(string value) => encryptionKey = value;
        public void SetUseBase64(bool value) => useBase64 = value;
        #endif
    }
}