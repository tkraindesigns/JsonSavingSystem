﻿using System;
using UnityEditor;
using UnityEngine;

namespace GameDev.tv.Saving.Json.Editors
{
    /// <summary>
    /// Custom editor for the <see cref="JsonSaveableEntity"/> component, providing a
    /// user interface in the Unity Editor for managing the entity's identifier.
    /// </summary>
    [UnityEditor.CustomEditor(typeof(JsonSaveableEntity))]
    public class JsonSaveableEntityEditor : Editor
    {
        
        /// <summary>
        /// A serialized property representing a unique identifier for the JSON saveable entity.
        /// The identifier is used to ensure that each entity can be uniquely identified and managed.
        /// A note on SerializedProperty.  For best results, always make sure that the property's name
        /// is exactly what it is in the class (case sensitive!).  This will ensure that it's easy
        /// to look up the property in the SerializedObject using the nameof() feature.
        /// </summary>
        private SerializedProperty identifier; 
        
        /// <summary>
        /// Holds a reference to the JsonSaveableEntity instance for the editor script.
        /// </summary>
        private JsonSaveableEntity entity;

        /// <summary>
        /// A boolean variable indicating whether the identifier of the JsonSaveableEntity must be changed.
        /// </summary>
        private bool mustChange;

        /// <summary>
        /// This method is called when the editor becomes enabled and active.
        /// It initializes the serialized property and references to the associated JsonSaveableEntity,
        /// and checks if the entity's identifier needs to be changed to ensure uniqueness
        /// and avoid conflicts in the Unity Editor.
        /// </summary>
        private void OnEnable()
        {
            identifier = serializedObject.FindProperty(nameof(identifier));
            entity = target as JsonSaveableEntity;
            if (!IsPrefab() && !IsUnique(identifier.stringValue))
            {
                mustChange = true;
            }
        }

        /// <summary>
        /// Custom inspector GUI for the JsonSaveableEntity component. This method handles the display and management
        /// of the identifier field in Unity's inspector. It ensures that the identifier field is unique and not empty.
        /// If the component is a prefab, it clears the identifier. If it's an instance in the scene, it provides a
        /// text field for user input and a mechanism to ensure uniqueness and validity of the identifier.
        /// </summary>
        public override void OnInspectorGUI()
        {
            if (IsPrefab())
            {
                if (identifier.stringValue != "")
                {
                    identifier.stringValue = "";
                }
            }
            else
            {
                EditorGUI.BeginChangeCheck();
                string newValue = EditorGUILayout.TextField("ID", identifier.stringValue);
                if (EditorGUI.EndChangeCheck() || string.IsNullOrWhiteSpace(identifier.stringValue) || mustChange)
                {
                    if (IsUnique(newValue))
                    {
                        identifier.stringValue = newValue;
                    }
                    else
                    {
                        identifier.stringValue = System.Guid.NewGuid().ToString();
                    }
                    mustChange = false;
                }
            }
            serializedObject.ApplyModifiedProperties();
        }

        /// <summary>
        /// Determines if the current <see cref="JsonSaveableEntity"/> is a prefab.
        /// </summary>
        /// <returns>True if the entity is a prefab; otherwise, false.</returns>
        bool IsPrefab()
        {
            return !entity.gameObject.scene.IsValid();
        }

        /// <summary>
        /// Checks if the provided identifier is unique among all instances of JsonSaveableEntity within the scene.
        /// Note that if you copy and paste a character from one scene to another, this method may not catch the
        /// change.  It's best instead to prefab your characters and bring them in from the assets, which would
        /// always ensure that a new identifier is generated.
        /// </summary>
        /// <param name="value">The identifier string to be checked for uniqueness.</param>
        /// <returns>True if the identifier is unique; otherwise, false.</returns>
        bool IsUnique(string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return false;
            foreach (JsonSaveableEntity saveableEntity in FindObjectsByType<JsonSaveableEntity>(FindObjectsSortMode.None))
            {
                if (saveableEntity.GetIdentifier() == value)
                {
                    if (object.ReferenceEquals(entity, saveableEntity)) continue;
                    return false;
                }
            }
            return true;
        }
    }
}