using GameDev.tv.Saving.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using UnityEngine;

public class Vector3ToJson
{
        
    [TestCase(5,3,5)]
    [TestCase(-5f,5f,0)]
    [TestCase(9006.3f, -1007.4f, 867.5309f)]
    public void Vector3JTokenConversions(float x, float y, float z)
    {
        Vector3 testVector = new Vector3(x, y, z);
        JToken jToken = testVector.ToToken();
        Assert.AreEqual(testVector, jToken.ToVector3());
        Assert.Pass($"{jToken}");
    }
    
    [TestCase("This is garbage")]
    public void Vector3_HandlesBadToken(string token)
    {
        JToken jToken = token;
        Assert.DoesNotThrow(() =>
        {
            Debug.Log($"{token} = {jToken.ToVector3()}");
        });
    }

    [TestCase(1, 2, 3)]
    public void Vector3_TestDoesNotThrow_XY(float x, float y, float z)
    {
        JToken token = new JObject() { ["x"] = x, ["y"] = y };
        Assert.DoesNotThrow(() =>
        {
            Debug.Log($"{token} = {token.ToVector3()}");
        });
    }

    [TestCase(1, 2, 3)]
    public void Vector3_TestEquality_XY(float x, float y, float z)
    {
        JToken token = new JObject() { ["x"] = x, ["y"] = y };
        Assert.AreEqual(new Vector3(x, y, 0), token.ToVector3());
    }

    [TestCase(1, 2, 3)]
    public void Vector3_TestDoesNotThrow_XZ(float x, float y, float z)
    {
        JToken token = new JObject() { ["x"] = x, ["z"] = z };
        Assert.DoesNotThrow(() =>
        {
            Debug.Log($"{token} = {token.ToVector3()}");
        });
    }

    [TestCase(1, 2, 3)]
    public void Vector3_TestEquality_XZ(float x, float y, float z)
    {
        JToken token = new JObject() { ["x"] = x, ["z"] = z };
        Assert.AreEqual(new Vector3(x, 0, z), token.ToVector3());
    }

    [TestCase(1, 2, 3)]
    public void Vector3_TestDoesNotThrow_YZ(float x, float y, float z)
    {
        JToken token = new JObject() { ["y"] = y, ["z"] = z };
        Assert.DoesNotThrow(() =>
        {
            Debug.Log($"{token} = {token.ToVector3()}");
        });
    }

    [TestCase(1, 2, 3)]
    public void Vector3_TestEquality_YZ(float x, float y, float z)
    {
        JToken token = new JObject() { ["y"] = y, ["z"] = z };
        Assert.AreEqual(new Vector3(0, y, z), token.ToVector3());
    }

    [TestCase(1, 2, 3)]
    public void Vector3_TestDoesNotThrow_None(float x, float y, float z)
    {
        JToken token = new JObject();
        Assert.DoesNotThrow(() =>
        {
            Debug.Log($"{token} = {token.ToVector3()}");
        });
    }

    [TestCase(1, 2, 3)]
    public void Vector3_TestEquality_None(float x, float y, float z)
    {
        JToken token = new JObject();
        Assert.AreEqual(Vector3.zero, token.ToVector3());
    }

    [TestCase(1, 2, 3)]
    public void Vector3_TestDoesNotThrow_BadParameter_X(float x, float y, float z)
    {
        JToken token = new JObject() { ["x"] = "Garbage", ["y"] = y, ["z"] = z };
        Assert.DoesNotThrow(() =>
        {
            Debug.Log($"{token} = {token.ToVector3()}");
        });
    }
    
    [TestCase(1, 2, 3)]
    public void Vector3_TestEquality_BadParameter_X(float x, float y, float z)
    {
        JToken token = new JObject() { ["x"] = "Garbage", ["y"] = y, ["z"] = z };
        Assert.AreEqual(new Vector3(0,y,z), token.ToVector3());
    }
    
    [TestCase(1, 2, 3)]
    public void Vector3_TestDoesNotThrow_BadParameter_Y(float x, float y, float z)
    {
        JToken token = new JObject() { ["x"] = x, ["y"] = "Garbage", ["z"] = z };
        Assert.DoesNotThrow(() =>
        {
            Debug.Log($"{token} = {token.ToVector3()}");
        });
    }

    [TestCase(1, 2, 3)]
    public void Vector3_TestEquality_BadParameter_Y(float x, float y, float z)
    {
        JToken token = new JObject() { ["x"] = x, ["y"] = "Garbage", ["z"] = z };
        Assert.AreEqual(new Vector3(x, 0, z), token.ToVector3());
    }

    [TestCase(1, 2, 3)]
    public void Vector3_TestDoesNotThrow_BadParameter_Z(float x, float y, float z)
    {
        JToken token = new JObject() { ["x"] = x, ["y"] = y, ["z"] = "Garbage" };
        Assert.DoesNotThrow(() =>
        {
            Debug.Log($"{token} = {token.ToVector3()}");
        });
    }

    [TestCase(1, 2, 3)]
    public void Vector3_TestEquality_BadParameter_Z(float x, float y, float z)
    {
        JToken token = new JObject() { ["x"] = x, ["y"] = y, ["z"] = "Garbage" };
        Assert.AreEqual(new Vector3(x, y, 0), token.ToVector3());
    }
}
