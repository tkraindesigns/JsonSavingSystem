using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class JArrayTests
{
    private const string StringOne = "String One";
    private const string StringTwo = "String Two";
    private const string StringThree = "String Three";
    private const string StringFour = "String Four";

    private List<string> stringItems;
    private List<int> intItems;
    [OneTimeSetUp]
    public void Setup()
    {
        stringItems = new List<string>()
        {
            StringOne,
            StringTwo,
            StringThree,
            StringFour
        };
        intItems = new List<int>()
        {
            1,
            2,
            3,
            5,
            7,
            11,
            13,
            17,
            19,
            23
        };
    }

    [OneTimeTearDown]
    public void TearDown()
    {
        stringItems.Clear();
        stringItems = null;
    }
    
    [Test]
    public void TestStringItems()
    {
        JArray jArray = new JArray();
        foreach (string item in stringItems)
        {
            jArray.Add(item);
        }
        JToken token = jArray;
        if (token is JArray tokenArray)
        {
            List<string> decodedStringItems = new();
            foreach (JToken jToken in tokenArray)
            {
                decodedStringItems.Add(jToken.ToString());
            }
            CollectionAssert.AreEqual(stringItems, decodedStringItems);
            Assert.Pass($"Strings successfully encoded/decoded {token}");
        } 
        Assert.Fail($"Token is not a JArray {token}");
    }

    [Test]
    public void TestIntItems()
    {
        JArray jArray = new JArray();
        foreach (int item in intItems)
        {
            jArray.Add(item);
        }
        JToken token = jArray;
        if (token is JArray tokenArray)
        {
            List<int> decodedIntItems = new();
            foreach (JToken jToken in tokenArray)
            {
                decodedIntItems.Add(jToken.ToObject<int>());
            }
            CollectionAssert.AreEqual(intItems, decodedIntItems);
            Assert.Pass($"Strings successfully encoded/decoded {token}");
        } 
        Assert.Fail($"Token is not a JArray {token}");
    }

    [TestCase(1,2,3)]
    [TestCase(867, 5, 309)]
    [TestCase(1215, 1776, 1783)]
    public void TestVariousItems(int x, int y, int z)
    {
        List<int> values = new List<int>() { x, y, z };
        JToken token = JToken.FromObject(values);
        if (token is JArray jArray)
        {
            CollectionAssert.AreEqual(values, jArray.ToObject<List<int>>(), $"{values} != {token}");
            Assert.Pass($"{token} ");
        }
        else
        {
            Assert.Fail($"{token} is not a JArray");
        }
    }
    
}
