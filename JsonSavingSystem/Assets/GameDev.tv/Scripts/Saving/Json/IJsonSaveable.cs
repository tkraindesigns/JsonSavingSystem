using Newtonsoft.Json.Linq;

namespace GameDev.tv.Saving.Json
{
    /// <summary>
    /// An interface for objects that can be saved in JSON format in the GameDev.tv Json Saving System.
    /// </summary>
    public interface IJsonSaveable
    {
        /// <summary>
        /// Captures the state the component as a <see cref="JToken"/>.
        /// </summary>
        /// <returns>A <see cref="JToken"/> object representing the captured state.</returns>
        /// <remarks>
        /// This method is used to convert the current state of the object to a JToken object.
        /// The JToken object can be a <see cref="JObject"/>, a <see cref="JArray"/>, or a simple <see cref="JValue"/>.
        /// </remarks>
        public JToken CaptureAsJToken();

        /// <summary>
        /// Restores the object's state from a <see cref="JToken"/>  It is up to RestoreState to properly
        /// decode the JToken and restore the state of the component..
        /// </summary>
        /// <param name="state">The <see cref="JToken"/> containing the object's state.</param>
        public void RestoreFromJToken(JToken state);
    }
}
