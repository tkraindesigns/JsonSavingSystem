using System;
using GameDev.tv.Saving.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

public class TestJValues
{
    [TestCase(true)]
    [TestCase(false)]
    public void EncodeDecodeBooleanValue(bool value)
    {
        JToken token = value;
        Assert.AreEqual(value, token.ToObject<bool>(), $"Fail:Token {token} is not {value}");
        Assert.Pass($"Token {token.ToString()} is {value}");
    }

    [TestCase(1)]
    [TestCase(-500)]
    [TestCase(32)]
    [TestCase(1000)]
    public void EndodeDecodeIntValue(int value)
    {
        JToken token = value;
        Assert.AreEqual(value, token.ToInt(), $"Fail:Token {token} is not {value}");
        Assert.Pass($"Token {token.ToString()} is {value}");
    }

    
    [TestCase("1")]
    [TestCase("garbage")]
    public void EncodeBadIntValue(string value)
    {
        JToken token = value;
        Assert.DoesNotThrow(()=> token.ToInt());
        Assert.Pass($"{value} creates token {token.ToString()}, ToInt()={token.ToInt()}");
    }

    [TestCase("1")]
    [TestCase("123.231")]
    [TestCase("garbage")]
    public void EncodeBadFloatValue(string value)
    {
        JToken token = value;
        Assert.DoesNotThrow(()=> token.ToFloat());
        Assert.Pass($"{value} creates token {token.ToString()}, ToFloat()={token.ToFloat()}");
    }
    
    [TestCase(3.14f)]
    [TestCase(-2.5f)]
    [TestCase(0.0f)]
    [TestCase(100.99f)]
    [TestCase(0.00000000000000001f)]
    [TestCase(1.00000000000000001f)]
    public void EncodeDecodeFloatValue(float value)
    {
        JToken token = value;
        Assert.AreEqual(value, token.ToFloat(), $"Fail:Token {token} is not {value}");
        Assert.Pass($"Token {token.ToString()} is {value}");
    }
    
    [TestCase("hello")]
    [TestCase("")]
    [TestCase("Rockstar")]
    public void EncodeDecodeStringValue(string value)
    {
        JToken token = value;
        Assert.AreEqual(value, token.ToObject<string>(), $"Fail:Token {token} is not {value}");
        Assert.Pass($"Token {token.ToString()} is {value}");
    }

    [TestCase("NOW")]
    [TestCase("2024-04-21 06:00:00")]
    [TestCase("2048-01-01 00:00:01")]
    [TestCase("04:30")]
    [TestCase("2021-04-30")]
    [TestCase("Not A DateTime")]
    public void EncodeDateTimeValue(string dateTime)
    {
        DateTime value = new DateTime();
        if (dateTime=="NOW")
        {
            value = DateTime.Now;
        }
        else
        {
            try
            {
                value = DateTime.Parse(dateTime);
            }
            catch (Exception e)
            {
                Assert.Ignore($"{dateTime} is not a valid DateTime.");
            }
        }
        JToken token = value;
        Assert.AreEqual(value, token.ToObject<DateTime>(), $"Fail:Token {token} is not {value}");
        Assert.Pass($"Token {token.ToString()} is {value}");
    }
    
}
