using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameDev.tv.Saving.Json
{
    public static class Utilities
    {
        /// <summary>
        /// Converts a JToken to a float value.
        /// </summary>
        /// <param name="state">The JToken to convert.</param>
        /// <param name="fallback">The fallback value to return if the conversion fails. Default is 0f.</param>
        /// <returns>The float value converted from the JToken. If the conversion fails, returns the fallback value.</returns>
        public static float ToFloat(this JToken state, float fallback = 0f) => float.TryParse(state.ToString(), out float f) ? f : fallback;


        /// <summary>
        /// Converts a JToken to an integer value.
        /// </summary>
        /// <param name="state">The JToken to convert.</param>
        /// <param name="fallback">The value to return if the conversion fails. Default is 0.</param>
        /// <returns>The integer value converted from the JToken. If the conversion fails or the JToken is not a valid representation of an integer, returns the fallback value.</returns>
        public static int ToInt(this JToken state, int fallback = 0) => int.TryParse(state.ToString(), out int i) ? i : fallback;


        /// <summary>
        /// Converts a Vector3 object to a JToken.
        /// </summary>
        /// <param name="vector3">The Vector3 object to convert.</param>
        /// <returns>A JToken representing the Vector3 object.</returns>
        public static JToken ToToken(this Vector3 vector3)
        {
            JObject state = new JObject
            {
                ["x"] = vector3.x,
                ["y"] = vector3.y,
                ["z"] = vector3.z
            };
            return state;
        }


        /// <summary>
        /// Converts a JToken to a Vector3 value.
        /// </summary>
        /// <param name="state">The JToken to convert.</param>
        /// <returns>The Vector3 value converted from the JToken. If the conversion fails or the JToken is not a valid representation of Vector3, returns a zero Vector3 (0, 0, 0).</returns>
        public static Vector3 ToVector3(this JToken state)
        {
            Vector3 result = new Vector3();
            if (state is JObject stateDict)
            {
                if(stateDict.TryGetValue("x", out JToken xValue))
                {
                    result.x = xValue.ToFloat();
                }
                if(stateDict.TryGetValue("y", out JToken yValue))
                {
                    result.y = yValue.ToFloat();
                }
                if(stateDict.TryGetValue("z", out JToken zValue))
                {
                    result.z = zValue.ToFloat();
                }
            }
            return result;
        }

        /// <summary>
        /// Converts a Vector2 to a JToken.
        /// </summary>
        /// <param name="vector2">The Vector2 to convert.</param>
        /// <returns>A JToken representing the converted Vector2.</returns>
        public static JToken ToToken(this Vector2 vector2)
        {
            JObject state = new JObject { ["x"] = vector2.x, ["y"] = vector2.y };
            return state;
        }

        /// <summary>
        /// Converts a JToken to a Vector2 value.
        /// </summary>
        /// <param name="state">The JToken to convert.</param>
        /// <returns>The Vector2 value converted from the JToken. If the conversion fails or the JToken is not a valid representation of Vector2, returns a zero Vector2 (0, 0).</returns>
        public static Vector2 ToVector2(this JToken state)
        {
            Vector2 result = Vector2.zero;
            if (state is JObject stateDict)
            {
                if(stateDict.TryGetValue("x", out JToken xValue))
                {
                    result.x = xValue.ToFloat();
                }
                if(stateDict.TryGetValue("y", out JToken yValue))
                {
                    result.y = yValue.ToFloat();
                }
            }
            return result;
        }

        /// <summary>
        /// Converts a Quaternion to a JToken.
        /// </summary>
        /// <param name="quaternion">The Quaternion to convert.</param>
        /// <returns>A JToken representing the Quaternion.</returns>
        public static JToken ToToken(this Quaternion quaternion)
        {
            JObject state = new JObject { ["x"] = quaternion.x , ["y"] = quaternion.y , ["z"] = quaternion.z, ["w"] = quaternion.w };
            return state;
        }

        /// <summary>
        /// Converts a JToken to a Quaternion object.
        /// </summary>
        /// <param name="state">The JToken to convert.</param>
        /// <returns>A Quaternion object converted from the JToken. If the conversion fails, returns the identity Quaternion.</returns>
        public static Quaternion ToQuaternion(this JToken state)
        {
            Quaternion result = Quaternion.identity;
            if (state is JObject stateDict)
            {
                if(stateDict.TryGetValue("x", out JToken xValue))
                {
                    result.x = xValue.ToFloat();
                }
                if(stateDict.TryGetValue("y", out JToken yValue))
                {
                    result.y = yValue.ToFloat();
                }
                if(stateDict.TryGetValue("z", out JToken zValue))
                {
                    result.z = zValue.ToFloat();
                }
                if(stateDict.TryGetValue("w", out JToken wValue))
                {
                    result.w = wValue.ToFloat();
                }
            }
            return result;
        }

        /// <summary>
        /// Converts a Vector3Int to a JToken.
        /// </summary>
        /// <param name="vector3Int">The Vector3Int to convert.</param>
        /// <returns>A JToken representing the Vector3Int.</returns>
        public static JToken ToToken(this Vector3Int vector3Int)
        {
            JObject state = new JObject { ["x"] = vector3Int.x, ["y"] = vector3Int.y, ["z"] = vector3Int.z };
            return state;
        }

        /// <summary>
        /// Converts a JToken to a Vector3Int object.
        /// </summary>
        /// <param name="state">The JToken to convert.</param>
        /// <returns>A Vector3Int object representing the JToken. If the conversion fails, returns a Vector3Int with all components set to 0.</returns>
        public static Vector3Int ToVector3Int(this JToken state)
        {
            Vector3Int result = new Vector3Int();
            if (state is JObject stateDict)
            {
                if(stateDict.TryGetValue("x", out JToken xValue))
                {
                    result.x = xValue.ToInt();
                }
                if(stateDict.TryGetValue("y", out JToken yValue))
                {
                    result.y = yValue.ToInt();
                }
                if(stateDict.TryGetValue("z", out JToken zValue))
                {
                    result.z = zValue.ToInt();
                }
            }
            return result;
        }
        

        /// <summary>
        /// Converts a <see cref="Vector2Int"/> object to a <see cref="JToken"/> object.
        /// </summary>
        /// <param name="vector2Int">The <see cref="Vector2Int"/> object to convert.</param>
        /// <returns>A <see cref="JToken"/> object representing the converted <see cref="Vector2Int"/> object.</returns>
        public static JToken ToToken(this Vector2Int vector2Int)
        {
            JObject state = new JObject { ["x"] = vector2Int.x, ["y"] = vector2Int.y };
            return state;
        }

        /// <summary>
        /// Converts a JToken to a Vector2Int value.
        /// </summary>
        /// <param name="state">The JToken to convert.</param>
        /// <returns>The Vector2Int value converted from the JToken. If the conversion fails or the JToken is not a valid representation of Vector2Int, returns a zero Vector2Int (0, 0).</returns>
        public static Vector2Int ToVector2Int(this JToken state)
        {
            Vector2Int result = new Vector2Int();
            if (state is JObject stateDict)
            {
                if(stateDict.TryGetValue("x", out JToken xValue))
                {
                    result.x = xValue.ToInt();
                }
                if(stateDict.TryGetValue("y", out JToken yValue))
                {
                    result.y = yValue.ToInt();
                }
            }
            return result;
        }
    }
}
