﻿using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameDev.tv.Saving.Json
{
    /// <summary>
    /// Represents a MonoBehaviour component that can be saved and restored in JSON format.
    /// </summary>
    public class JsonSaveableEntity : MonoBehaviour
    {
        /// <summary>
        /// The identifier of a JSON saveable entity.
        /// </summary>
        /// <remarks>
        /// This identifier is used to uniquely identify a JSON saveable entity.
        /// </remarks>
        [SerializeField] private string identifier = "";

        /// <summary>
        /// Gets the identifier of the JSON saveable entity.
        /// </summary>
        /// <returns>The identifier of the JSON saveable entity.</returns>
        public string GetIdentifier() => identifier;

        /// <summary>
        /// Captures the state of the entity as a <see cref="JToken"/> object.
        /// </summary>
        /// <returns>A <see cref="JToken"/> object representing the state of the entity.</returns>
        public JToken CaptureEntity()
        {
            JObject state = new();
            foreach (IJsonSaveable saveable in GetComponents<IJsonSaveable>())
            {
                state[saveable.GetType().ToString()] = saveable.CaptureAsJToken();
            }
            return state;
        }

        /// <summary>
        /// Restores the state of the entity from a <see cref="JToken"/>.
        /// </summary>
        /// <param name="state">The <see cref="JToken"/> containing the entity's state.</param>
        public void RestoreEntity(JToken state)
        {
            if (state is JObject stateDict)
            {
                foreach (IJsonSaveable saveable in GetComponents<IJsonSaveable>())
                {
                    if (stateDict.TryGetValue(saveable.GetType().ToString(), out JToken token))
                    {
                        saveable.RestoreFromJToken(token);
                    }
                }
            }
        }
    }
}