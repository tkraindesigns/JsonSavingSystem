﻿namespace GameDevTV.Utils
{
    public interface IHasUniqueIdentifier
    {
        UniqueIdentifierGO GetUniqueIdentifier();
    }
}