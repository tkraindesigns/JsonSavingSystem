﻿using UnityEditor;
using UnityEngine;

namespace GameDev.tv.Saving.Json.Editors
{
    public static class JsonSavingSystemSettingsProvider
    {
        [SettingsProvider]
        public static SettingsProvider CreateJsonSavingSystemSettingsProvider()
        {
            var provider = new SettingsProvider("Project/Json Saving System", SettingsScope.Project)
            {
                label = "Json Saving System",
                guiHandler = (searchContext) =>
                {
                    var settings = JsonSavingSystemSettings.Instance;
                    if (settings == null)
                    {
                        EditorGUILayout.HelpBox("JsonSavingSystemSettings Asset not found", MessageType.Error);
                        return;
                    }
                    
                    settings.SetFileExtension(EditorGUILayout.TextField("File Extension", settings.fileExtension));
                    settings.SetUseEncryption(EditorGUILayout.Toggle("Use Encryption", settings.useEncryption));
                    if (settings.useEncryption)
                    {
                        GUIStyle style = new GUIStyle();
                        style.padding = new RectOffset(20, 20, 0, 0);
                        EditorGUILayout.BeginHorizontal(style);
                        settings.SetEncryptionKey(EditorGUILayout.TextField("Encryption Key", settings.encryptionKey));
                        if (GUILayout.Button("Randomize"))
                        {
                            string newKey = System.Convert.ToBase64String(System.Guid.NewGuid().ToByteArray());
                            newKey=newKey+ System.Convert.ToBase64String(System.Guid.NewGuid().ToByteArray());
                            newKey = newKey.Replace("=", string.Empty);
                            settings.SetEncryptionKey(newKey);
                            EditorUtility.SetDirty(settings);
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                    settings.SetUseBase64(EditorGUILayout.Toggle("Use Base64", settings.useBase64));
                    if (GUI.changed)
                    {
                        EditorUtility.SetDirty(settings);
                    }
                }
                , titleBarGuiHandler = () =>
                {
                    EditorGUILayout.HelpBox("Json Saving System Settings", MessageType.Info);
                }
            };
            return provider;
        }
    }
}