using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameDevTV.Utils
{
    [System.Serializable]
    public class UniqueIdentifierGO
    {
        [SerializeField] private string uniqueIdentifier = "";

        public static implicit operator string(UniqueIdentifierGO uniqueIdentifierGoObject)
        {
            return uniqueIdentifierGoObject?.uniqueIdentifier;
        }
    }
}