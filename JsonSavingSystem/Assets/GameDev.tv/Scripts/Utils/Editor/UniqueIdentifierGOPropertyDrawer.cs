﻿using System.Linq;
using GameDevTV.Utils;
using UnityEditor;
using UnityEngine;

namespace GameDevTV.Utils.Editors
{
    [UnityEditor.CustomPropertyDrawer(typeof(UniqueIdentifierGO))]
    public class UniqueIdentifierGOPropertyDrawer : UnityEditor.PropertyDrawer
    {
        
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty uniqueIdentifier = property.FindPropertyRelative("uniqueIdentifier");
            if (property.serializedObject.targetObject is MonoBehaviour behaviour )
            {
                if (!behaviour.gameObject.scene.IsValid())
                {
                    GUI.Label(position, "Prefab Mode - Not Serialized");
                    uniqueIdentifier.stringValue = "";
                    return;
                }
                EditorGUI.BeginChangeCheck();
                string tempValue = EditorGUI.TextField(position, label, uniqueIdentifier.stringValue);
                if (EditorGUI.EndChangeCheck())
                {
                    if (IsGameObjectUnique(tempValue, behaviour.gameObject))
                    {
                        uniqueIdentifier.stringValue = tempValue;
                    }
                    else
                    {
                        uniqueIdentifier.stringValue = System.Guid.NewGuid().ToString();
                    }
                }
            }
        }

        private bool IsGameObjectUnique(string value, GameObject sourceObject)
        {
            if (string.IsNullOrWhiteSpace(value)) return false;
            foreach (GameObject testObject in GameObject.FindObjectsByType<GameObject>(FindObjectsSortMode.None))
            {
                if (testObject.TryGetComponent(out IHasUniqueIdentifier testIdentifier))
                {
                    if (testIdentifier.GetUniqueIdentifier() == value && testObject != sourceObject) return false;
                }
            }
            return true;
        }


    }
}