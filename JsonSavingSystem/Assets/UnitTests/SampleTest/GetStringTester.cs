using System.Collections.Generic;
using GameDev.tv.Saving.Json;
using NUnit.Framework;

public class GetStringTester
{
    private const string StringOne = "String One";
    private const string StringTwo = "String Two";
    private const string StringThree = "String Three";
    private const string StringFour = "String Four";

    private List<string> strings;
    [OneTimeSetUp]
    public void Setup()
    {
        strings = new List<string>
        {
            StringOne,
            StringTwo,
            StringThree,
            StringFour,
        };
    }

    [OneTimeTearDown]
    public void TearDown()
    {
        strings.Clear();
        strings = null;
    }

    [Test]
    public void HandlesNullList()
    {
        Assert.DoesNotThrow(()=>SampleTestClass.GetStringFromList(null, 0));
    }

    [Test]
    public void ReturnsEmptyStringWhenNull()
    {
        Assert.AreEqual("", SampleTestClass.GetStringFromList(null, 0));
    }

    [Test]
    public void HandlesValidIndex()
    {
        Assert.AreEqual(StringTwo, SampleTestClass.GetStringFromList(strings, 1));
    }

    [Test]
    public void HandlesNegativeIndex()
    {
        Assert.AreEqual("", SampleTestClass.GetStringFromList(strings, -1));
    }

    [Test]
    public void HandlesIndexToLarge()
    {
        Assert.AreEqual("", SampleTestClass.GetStringFromList(strings, strings.Count));
    }
}
