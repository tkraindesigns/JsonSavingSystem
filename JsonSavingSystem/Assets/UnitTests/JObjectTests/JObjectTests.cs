using System.Collections.Generic;
using GameDev.tv.Saving.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using UnityEngine;

public class JObjectTests
{

    public struct StudentRecord
    {
        public int ID;
        public string Name;

        public static StudentRecord CloneRecord(StudentRecord record)
        {
            return record;
        }
        [JsonIgnore]
        public StudentRecord Clone => CloneRecord(this);

    }
    
    [TestCase(1, "Ben Tristem" )]
    [TestCase(2, "Rick Davidson")]
    [TestCase(3, "Brian Trotter")]
    public void EncodeDecodeStudentRecord(int id, string name)
    {
        StudentRecord record = new StudentRecord()
        {
            ID=id,
            Name=name
        };
        JToken token = JObject.FromObject(record);
        StudentRecord newRecord = token.ToObject<StudentRecord>();
        Assert.AreEqual(record.ID, newRecord.ID, $"Ids do not match {record.ID}, {newRecord.ID}, {token}");
        Assert.AreEqual(record.Name, newRecord.Name, $"Names do not match {record.Name}, {newRecord.Name}, {token}");
        Assert.Pass($"{token}");
    }
    
    private Dictionary<int, string> students = new();
    
    [TestCase(1, "Ben Tristem" )]
    [TestCase(2, "Rick Davidson")]
    [TestCase(3, "Brian Trotter")]
    public void EncodeDecodeStudentDictionary(int id, string name)
    {
        students[id] = name;
        JToken token = JObject.FromObject(students);
        Dictionary<int, string> decodedStudents = token.ToObject<Dictionary<int, string>>();
        CollectionAssert.AreEqual(students, decodedStudents);
        Assert.Pass($"{token}");
    }
    
    
    private Dictionary<int, StudentRecord> studentRecords = new();
    
    [TestCase(1, "Ben Tristem" )]
    [TestCase(2, "Rick Davidson")]
    [TestCase(3, "Brian Trotter")]
    public void EncodeDecodeStudentRecordDictionary(int id, string name)
    {
        studentRecords[id] = new StudentRecord() {ID = id, Name = name};
        JToken token = JObject.FromObject(studentRecords);
        Dictionary<int, StudentRecord> decodedStudents = token.ToObject<Dictionary<int, StudentRecord>>();
        CollectionAssert.AreEqual(studentRecords, decodedStudents);
        Assert.Pass($"{token}");
    }

    
    [TestCase(5,3,5)]
    [TestCase(-5f,5f,0)]
    [TestCase(9006.3f, -1007.4f, 867.5309f)]
    public void Vector3JTokenConversions(float x, float y, float z)
    {
        Vector3 testVector = new Vector3(x, y, z);
        JToken jToken = testVector.ToToken();
        Assert.AreEqual(testVector, jToken.ToVector3());
        Assert.Pass($"{jToken}");
    }
}
