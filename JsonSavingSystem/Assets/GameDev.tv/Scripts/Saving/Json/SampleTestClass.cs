using System.Collections.Generic;

namespace GameDev.tv.Saving.Json
{
    public static class SampleTestClass
    {
        public static string GetStringFromList(List<string> strings, int index)
        {
            if (strings!=null)
            {
                if (index >= 0)
                {
                    if (index < strings.Count)
                    {
                        return strings[index];
                    }
                }
            }

            return "";
        }

    }
}
